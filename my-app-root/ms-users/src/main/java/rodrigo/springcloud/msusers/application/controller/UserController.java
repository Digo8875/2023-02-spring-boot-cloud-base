package rodrigo.springcloud.msusers.application.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.RequiredArgsConstructor;
import rodrigo.springcloud.msusers.application.representation.UserDTO;
import rodrigo.springcloud.msusers.application.representation.UserDTOValidation;
import rodrigo.springcloud.msusers.application.representation.UserDTO.UserViews;
import rodrigo.springcloud.msusers.application.representation.UserDTOValidation.UserValidations;
import rodrigo.springcloud.msusers.application.service.UserService;

@RestController
@RequestMapping(
    value = "/users",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class UserController {
    
    private final Environment environment;
    private final UserService service;

    @PostMapping
    @JsonView(UserViews.Detailed.class)
    public ResponseEntity create(@Validated({UserValidations.Create.class}) @RequestBody UserDTOValidation request) {

        UserDTO newUser = service.create(request);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{email}")
            .buildAndExpand(newUser.getEmail())
            .toUri();

        return ResponseEntity.created(location).body(newUser);
    }

    @GetMapping("/{uuid}")
    @JsonView(UserViews.Detailed.class)
    public ResponseEntity getByUuid(@PathVariable UUID uuid) {

        UserDTO user = service.getByUuid(uuid);
        
        return ResponseEntity.ok(user);
    }

    @PutMapping("/{uuid}")
    @JsonView(UserViews.Detailed.class)
    public ResponseEntity updateUser(
        @PathVariable UUID uuid,
        @Validated({UserValidations.Update.class}) @RequestBody UserDTOValidation request
    ) {

        UserDTO updatedUser = service.update(uuid, request);

        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{uuid}")
    @JsonView(UserViews.Detailed.class)
    public ResponseEntity deleteUser(@PathVariable UUID uuid) {

        service.softDelete(uuid);

        Map<String, String> response = new HashMap<>();
        response.put("message", "User deleted successfully");

        return ResponseEntity.ok(response);
    }
}
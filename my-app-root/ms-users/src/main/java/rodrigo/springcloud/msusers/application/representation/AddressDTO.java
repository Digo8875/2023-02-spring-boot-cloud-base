package rodrigo.springcloud.msusers.application.representation;

import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AddressDTO {
    
    @JsonView({AddressViews.Detailed.class})
    private Timestamp createdAt;

    @JsonView({AddressViews.Detailed.class})
    private Timestamp updatedAt;

    @JsonView({AddressViews.Detailed.class})
    private Timestamp deletedAt;

    @JsonView({AddressViews.Detailed.class})
    private UUID id;

    @JsonView({AddressViews.Resume.class})
    private String street;

    @JsonView({AddressViews.Resume.class})
    private String neighborhood;

    @JsonView({AddressViews.Resume.class})
    private String city;

    @JsonView({AddressViews.Resume.class})
    private String state;

    @JsonView({AddressViews.Resume.class})
    private String country;

    @JsonView({AddressViews.Resume.class})
    private String zipCode;

    @JsonView({AddressViews.Resume.class})
    private String complement;


    public class AddressViews{
        public interface Resume {}
        public interface Detailed extends Resume {}
    }
}

package rodrigo.springcloud.msusers.application.representation;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class AddressDTOValidation {
    
    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 255, 
        message = "Max 255 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String street;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 100, 
        message = "Max 100 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String neighborhood;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 100, 
        message = "Max 100 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String city;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 50, 
        message = "Max 50 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String state;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 100, 
        message = "Max 100 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String country;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 20, 
        message = "Max 20 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String zipCode;

    @NotBlank(
        message = "Required", 
        groups = AddressValidations.Upsert.class
    )
    @Size(
        max = 100, 
        message = "Max 100 characters", 
        groups = AddressValidations.Upsert.class
    )
    private String complement;

    
    public class AddressValidations{
        public interface Upsert {}
        public interface Create extends Upsert {}
        public interface Update extends Upsert {}
    }
}
package rodrigo.springcloud.msusers.application.representation;

import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import rodrigo.springcloud.msusers.application.representation.AddressDTO.AddressViews;
import rodrigo.springcloud.msusers.domain.User.Gender;

@Data
@RequiredArgsConstructor
public class UserDTO {
    
    @JsonView({UserViews.Detailed.class})
    private Timestamp createdAt;

    @JsonView({UserViews.Detailed.class})
    private Timestamp updatedAt;

    @JsonView({UserViews.Detailed.class})
    private Timestamp deletedAt;
    
    @JsonView({UserViews.Detailed.class})
    private UUID id;

    @JsonView({UserViews.Resume.class})
    private String name;

    @JsonView({UserViews.Resume.class})
    private String surname;

    @JsonView({UserViews.Resume.class})
    private Gender gender;

    @JsonView({UserViews.Resume.class})
    private String email;

    @JsonView({UserViews.Resume.class})
    private Boolean programmer;

    @JsonView({UserViews.Detailed.class})
    private String keycloakId;

    @JsonView({UserViews.WithAddress.class})
    private AddressDTO address;

    
    public class UserViews{
        public interface Resume {}
        public interface WithAddress {}
        public interface ResumeWithAddress extends Resume, WithAddress, AddressViews.Resume {}
        public interface Detailed extends Resume, WithAddress, AddressViews.Detailed {}
    }
}

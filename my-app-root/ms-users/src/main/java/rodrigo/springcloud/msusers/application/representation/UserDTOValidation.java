package rodrigo.springcloud.msusers.application.representation;

import java.sql.Date;
import java.time.LocalDate;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.validation.constraints.Pattern.Flag;
import lombok.Data;
import rodrigo.springcloud.msusers.application.representation.AddressDTOValidation.AddressValidations;
import rodrigo.springcloud.msusers.domain.User.Gender;

@Data
public class UserDTOValidation {
    
    @NotBlank(
        message = "Required", 
        groups = UserValidations.Create.class
    )
    @Size(
        max = 150, 
        message = "Max 150 characters", 
        groups = UserValidations.Create.class
    )
    private String name;

    @NotBlank(
        message = "Required", 
        groups = UserValidations.Create.class
    )
    @Size(
        max = 250, 
        message = "Max 250 characters", 
        groups = UserValidations.Create.class
    )
    private String surname;

    @NotNull(
        message = "Required", 
        groups = UserValidations.Create.class
    )
    private LocalDate birthdayDate;

    @NotNull(
        message = "Required", 
        groups = UserValidations.Upsert.class
    )
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotBlank(
        message = "Required", 
        groups = UserValidations.Upsert.class
    )
    @Email(
        message = "Invalid", 
        flags = { Flag.CASE_INSENSITIVE }, 
        groups = UserValidations.Upsert.class
    )
    @Size(
        max = 150, 
        message = "Max 150 characters", 
        groups = UserValidations.Upsert.class
    )
    private String email;

    @NotNull(
        message = "Required", 
        groups = UserValidations.Upsert.class
    )
    private Boolean programmer;

    @NotBlank(
        message = "Required", 
        groups = UserValidations.Create.class
    )
    @Size(
        max = 255, 
        message = "Max 255 characters", 
        groups = UserValidations.Create.class
    )
    private String password;

    @Valid
    @NotNull(
        message = "Required", 
        groups = UserValidations.WithAdress.class
    )
    private AddressDTOValidation address;


    public Date getBirthdayDate() {
        return Date.valueOf(this.birthdayDate);
    }
    
    
    public class UserValidations{
        public interface Upsert {}
        public interface WithAdress {}
        public interface Create extends Upsert, WithAdress, AddressValidations.Create {}
        public interface Update extends Upsert {}
    }
}

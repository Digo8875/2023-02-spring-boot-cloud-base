package rodrigo.springcloud.msusers.application.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import rodrigo.springcloud.msusers.application.representation.UserDTO;
import rodrigo.springcloud.msusers.application.representation.UserDTOValidation;
import rodrigo.springcloud.msusers.domain.Address;
import rodrigo.springcloud.msusers.domain.User;
import rodrigo.springcloud.msusers.http.client.KeycloakRestApiClient;
import rodrigo.springcloud.msusers.infra.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {
    
    private final Environment environment;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Transactional
    public UserDTO create(UserDTOValidation request) {

        Optional<User> existingUser = userRepository.findByEmail(request.getEmail());

        if (existingUser.isPresent()) {
            throw new EntityExistsException("User already registered");
        }

        User user = modelMapper.map(request, User.class);
        Address address = modelMapper.map(request.getAddress(), Address.class);
        user.setAddress(address);
        
        KeycloakRestApiClient keycloakClient = new KeycloakRestApiClient(environment);
        var keycloakAdminResponse = keycloakClient.getAdminToken();

        ResponseEntity keycloakResponse = keycloakClient.createUser(
            keycloakAdminResponse.get("access_token").toString(),
            user
        );

        String headerLocation = keycloakResponse.getHeaders().getLocation().toString();
        String splitLocation[] = headerLocation.split("/");
        String keycloakUserId = splitLocation[splitLocation.length - 1];

        try {
            user.setKeycloakId(keycloakUserId);

            User newUser = userRepository.save(user);

            return modelMapper.map(newUser, UserDTO.class);

        } catch (Exception e) {
            ResponseEntity keycloakDeleteResponse = keycloakClient.deleteUser(
                keycloakAdminResponse.get("access_token").toString(),
                keycloakUserId
            );

            throw e;
        }
    }

    public UserDTO getByUuid(UUID uuid) {
        Optional<User> user = userRepository.findByIdAndDeletedAtIsNullWithFetchAddress(uuid);

        if (user.isEmpty()) {
            throw new EntityNotFoundException("User not found");
        }

        return modelMapper.map(user.get(), UserDTO.class);
    }

    @Transactional
    public UserDTO update(UUID uuid, UserDTOValidation request){

        Optional<User> currentUser = userRepository.findByIdAndDeletedAtIsNull(uuid);
        Optional<User> existingUserWithRequestedEmail = userRepository.findByEmail(request.getEmail());

        if (currentUser.isEmpty()) {
            throw new EntityNotFoundException("User not found");
        }

        if (
            existingUserWithRequestedEmail.isPresent()
            && (currentUser.get().getId() != existingUserWithRequestedEmail.get().getId())
        ) {
            throw new EntityExistsException("Requested email already registered by another user");
        }
        
        User userToSave = currentUser.get();
        userToSave.setEmail(request.getEmail());
        userToSave.setGender(request.getGender());
        userToSave.setProgrammer(request.getProgrammer());

        User updatedUser = userRepository.save(userToSave);

        if (currentUser.get().getEmail() != request.getEmail()) {
            KeycloakRestApiClient keycloakClient = new KeycloakRestApiClient(environment);
            var keycloakAdminResponse = keycloakClient.getAdminToken();

            var keycloakResponse = keycloakClient.updateUser(
                keycloakAdminResponse.get("access_token").toString(),
                updatedUser.getKeycloakId(),
                updatedUser
            );
        }

        return modelMapper.map(updatedUser, UserDTO.class);
    }

    @Transactional
    public UserDTO softDelete(UUID uuid){

        Optional<User> currentUser = userRepository.findByIdAndDeletedAtIsNull(uuid);

        if (currentUser.isEmpty()) {
            throw new EntityNotFoundException("User not found");
        }

        Timestamp deletedAt = Timestamp.valueOf(LocalDateTime.now());
        
        User userToDelete = currentUser.get();
        userToDelete.setDeletedAt(deletedAt);

        User deletedUser = userRepository.save(userToDelete);

        KeycloakRestApiClient keycloakClient = new KeycloakRestApiClient(environment);
        var keycloakAdminResponse = keycloakClient.getAdminToken();

        var keycloakResponse = keycloakClient.deleteUser(
            keycloakAdminResponse.get("access_token").toString(),
            deletedUser.getKeycloakId()
        );

        return modelMapper.map(deletedUser, UserDTO.class);
    }
}
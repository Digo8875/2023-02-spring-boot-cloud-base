package rodrigo.springcloud.msusers.domain;

import java.sql.Date;
import java.util.UUID;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(
    name = "users", 
    uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) }
)
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User extends AbstractEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(updatable = false, unique = true)
    private UUID id;

    @Column(length = 100, nullable = false)
    private String name;
    
    @Column(length = 250, nullable = false)
    private String surname;

    @Column(name = "birthday_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthdayDate;

    @Column(length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(length = 250, unique = true, nullable = false)
    private String email;

    @Column(name = "programmer", columnDefinition = "BOOLEAN NOT NULL DEFAULT FALSE")
    private Boolean programmer;

    @JsonIgnore
    private String password;

    @JsonIgnore
    @Column(name = "keycloak_id", length = 50, nullable = false, unique = true)
    private String keycloakId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;


    public enum Gender {
        MALE("Male"),
        FEMALE("Female"),
        OTHER("Other"),
        NOT_INFORM("Not Inform");

        public String text;

        Gender (String gender) {
            text = gender;
        }
    }
}

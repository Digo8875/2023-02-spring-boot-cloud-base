package rodrigo.springcloud.msusers.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
  
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex, 
        HttpHeaders headers, 
        HttpStatusCode status, 
        WebRequest request
    ) {
        Map<String, Object> response = new HashMap<>();
        Map<String, String> fields = new HashMap<>();

        BindingResult bindingResult = ex.getBindingResult();

        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            for (FieldError error : errors) {
                fields.put(error.getField(), error.getDefaultMessage());
            }
        }

        response.put("error", fields);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        HttpMessageNotReadableException ex, 
        HttpHeaders headers, 
        HttpStatusCode status, 
        WebRequest request
    ) { 
        Map<String, Object> response = new HashMap<>();
        Map<String, Object> fields = new HashMap<>();

        InvalidFormatException invalidFormatEx = (InvalidFormatException) ex.getCause();
        String fieldName = invalidFormatEx.getPath().get(0).getFieldName();
        Class<?> targetClass = invalidFormatEx.getTargetType();

        if (targetClass.isEnum()) {
            Object[] enumConstants = targetClass.getEnumConstants();
            Map<String, Object> fieldInfos = new HashMap<>();
            String expectedEnums = "";

            for (int i = 0 ; i < enumConstants.length ; i++) {
                if (i == 0) {
                    expectedEnums = expectedEnums + enumConstants[i].toString();
                    continue;
                }
                expectedEnums = expectedEnums + "|" + enumConstants[i].toString();
            }

            fieldInfos.put("message", "Invalid value: " + invalidFormatEx.getValue());
            fieldInfos.put("expected", expectedEnums);
            fields.put(fieldName, fieldInfos);
        }

        if (fields.isEmpty()) {
            Map<String, Object> unmappedInfo = new HashMap<>();
            unmappedInfo.put("message", "Unmapped CLASS to handle in the exception");
            unmappedInfo.put("exception", "HttpMessageNotReadableException");
            unmappedInfo.put("class", targetClass.toString().substring(targetClass.toString().lastIndexOf('.') + 1));

            fields = unmappedInfo;
        } 

        response.put("error", fields);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatchException(
        MethodArgumentTypeMismatchException ex, 
        WebRequest request
    ) {
        Map<String, Object> response = new HashMap<>();
        Map<String, Object> informations = new HashMap<>();

        MethodParameter parameter = ex.getParameter();

        informations.put("message", "Type Mismatch");
        informations.put("parameter", parameter.getParameterName());
        informations.put("expected", parameter.getParameterType());

        response.put("error", informations);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WebClientException.class)
    protected ResponseEntity<Object> handleWebClientException(
        WebClientException ex, 
        WebRequest request
    ) {
        Map<String, String> body = new HashMap<>();
        body.put("error", "WebClient error: " + ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(KeycloakWebClientException.class)
    protected ResponseEntity<Object> handleKeycloakWebClientException(
        KeycloakWebClientException ex, 
        WebRequest request
    ) {
        Map<String, String> body = new HashMap<>();
        body.put("error", ex.formatMessageByStatus());

        return new ResponseEntity<>(body, ex.getStatus());
    }
    
    @ExceptionHandler(EntityExistsException.class)
    protected ResponseEntity<Object> handleEntityExistsException(
        EntityExistsException ex, 
        WebRequest request
    ) {
        Map<String, String> body = new HashMap<>();
        body.put("error", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(
        EntityNotFoundException ex, 
        WebRequest request
    ) {
        Map<String, String> body = new HashMap<>();
        body.put("error", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handlException(
        Exception ex, 
        WebRequest request
    ) {
        Map<String, String> body = new HashMap<>();
        body.put("error", "Generic Exception - INTERNAL_SERVER_ERROR");

        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
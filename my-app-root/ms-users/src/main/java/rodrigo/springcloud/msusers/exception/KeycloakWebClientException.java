package rodrigo.springcloud.msusers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientException;

public class KeycloakWebClientException extends WebClientException{

    private HttpStatus status;

    private final String KEYCLOAK_CONFLICT = "Resource already exists in Keycloak";
    private final String KEYCLOAK_NOT_FOUND = "Resource not found in Keycloak";
    private final String KEYCLOAK_BAD_REQUEST = "Invalid request to the Keycloak";
    private final String KEYCLOAK_FORBIDDEN_OR_UNAUTHORIZED = "Access denied by the Keycloak";
    private final String KEYCLOAK_CLIENT_DEFAULT = "CLIENT ERROR when connecting to the Keycloak";
    private final String KEYCLOAK_SERVER_DEFAULT = "SERVER ERROR when connecting to the Keycloak";

    public KeycloakWebClientException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String formatMessageByStatus() {

        if (this.status == HttpStatus.CONFLICT) {
            return KEYCLOAK_CONFLICT;
        }

        if (this.status == HttpStatus.NOT_FOUND) {
            return KEYCLOAK_NOT_FOUND;
        }

        if (this.status == HttpStatus.BAD_REQUEST) {
            return KEYCLOAK_BAD_REQUEST;
        }

        if (this.status == HttpStatus.FORBIDDEN || this.status == HttpStatus.UNAUTHORIZED) {
            return KEYCLOAK_FORBIDDEN_OR_UNAUTHORIZED;
        }

        if (this.status.is4xxClientError()) {
            return KEYCLOAK_CLIENT_DEFAULT;
        }

        return KEYCLOAK_SERVER_DEFAULT;
    }
}

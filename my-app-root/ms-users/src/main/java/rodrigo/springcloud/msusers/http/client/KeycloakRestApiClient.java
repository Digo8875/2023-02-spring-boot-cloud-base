package rodrigo.springcloud.msusers.http.client;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.RequiredArgsConstructor;
import rodrigo.springcloud.msusers.domain.User;
import rodrigo.springcloud.msusers.exception.KeycloakWebClientException;

public class KeycloakRestApiClient {
    
    private final Environment environment;

    private String keycloakAdmin;
    private String keycloakAdminPassword;
    private String clientId;
    private String realmName;

    public KeycloakRestApiClient(Environment environment) {
        this.environment = environment;
        this.keycloakAdmin = environment.getProperty("env.keycloak.admin");
        this.keycloakAdminPassword = environment.getProperty("env.keycloak.admin-password");
        this.clientId = environment.getProperty("env.keycloak.admin-client-id");
        this.realmName = environment.getProperty("env.keycloak.realm-name");
    }

    // ### DOCKER
    // private String baseUrl = "http://keycloak:8080";
    // ### LOCALHOST
    private String baseUrl = "http://localhost:8081";

    public HashMap getAdminToken() {
        
        String uri = "/realms/master/protocol/openid-connect/token";
        WebClient webClient = WebClient.create(baseUrl);

        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
        bodyValues.add("username", keycloakAdmin);
        bodyValues.add("password", keycloakAdminPassword);
        bodyValues.add("grant_type", "password");
        bodyValues.add("client_id", clientId);

        return webClient.post()
            .uri(uri)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromFormData(bodyValues))
            .retrieve()
            .onStatus(status -> status.is4xxClientError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Client Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .onStatus(status -> status.is5xxServerError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Server Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .bodyToMono(HashMap.class)
            .block();
    }

    public ResponseEntity createUser(
        String token,
        User user
    ) {
        String uri = "/admin/realms/" + realmName + "/users";
        WebClient webClient = WebClient.create(baseUrl);

        Map<String, Object> credential = new HashMap<>();
        credential.put("temporary", false);
        credential.put("type", "password");
        credential.put("userLabel", "My password");
        credential.put("value", user.getPassword());
        Object[] credentialsList = {credential};

        Map<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("credentials", credentialsList);
        bodyValues.put("email", user.getEmail());
        bodyValues.put("emailVerified", false);
        bodyValues.put("enabled", true);
        bodyValues.put("firstName", user.getName());
        bodyValues.put("lastName", user.getSurname());
        bodyValues.put("username", user.getEmail());

        return webClient.post()
            .uri(uri)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .headers(h -> h.setBearerAuth(token))
            .body(BodyInserters.fromValue(bodyValues))
            .retrieve()
            .onStatus(status -> status.is4xxClientError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Client Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .onStatus(status -> status.is5xxServerError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Server Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .toEntity(HashMap.class)
            .block();
    }

    public ResponseEntity updateUser(
        String token,
        String userId,
        User user
    ) {
        String uri = "/admin/realms/" + realmName + "/users/" + userId;
        WebClient webClient = WebClient.create(baseUrl);

        Map<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("email", user.getEmail());
        bodyValues.put("username", user.getEmail());

        return webClient.put()
            .uri(uri)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .headers(h -> h.setBearerAuth(token))
            .body(BodyInserters.fromValue(bodyValues))
            .retrieve()
            .onStatus(status -> status.is4xxClientError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Client Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .onStatus(status -> status.is5xxServerError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Server Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .toEntity(HashMap.class)
            .block();
    }

    public ResponseEntity deleteUser(
        String token,
        String userId
    ) {
        String uri = "/admin/realms/" + realmName + "/users/" + userId;
        WebClient webClient = WebClient.create(baseUrl);

        return webClient.delete()
            .uri(uri)
            .accept(MediaType.APPLICATION_JSON)
            .headers(h -> h.setBearerAuth(token))
            .retrieve()
            .onStatus(status -> status.is4xxClientError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Client Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .onStatus(status -> status.is5xxServerError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Server Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .toEntity(HashMap.class)
            .block();
    }

    public ResponseEntity disableUser(
        String token,
        String userId
    ) {
        String uri = "/admin/realms/" + realmName + "/users/" + userId;
        WebClient webClient = WebClient.create(baseUrl);

        Map<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("enabled", false);

        return webClient.put()
            .uri(uri)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .headers(h -> h.setBearerAuth(token))
            .body(BodyInserters.fromValue(bodyValues))
            .retrieve()
            .onStatus(status -> status.is4xxClientError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Client Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .onStatus(status -> status.is5xxServerError(), clientResponse -> {
                HttpStatus status = HttpStatus.valueOf(clientResponse.statusCode().value());
                String message = "Keycloak Server Exception";
                throw new KeycloakWebClientException(message, status);
            })
            .toEntity(HashMap.class)
            .block();
    }
}

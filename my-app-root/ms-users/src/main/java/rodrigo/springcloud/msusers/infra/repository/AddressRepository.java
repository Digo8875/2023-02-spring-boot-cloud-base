package rodrigo.springcloud.msusers.infra.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import rodrigo.springcloud.msusers.domain.Address;

public interface AddressRepository extends JpaRepository<Address, UUID> {
    
}

package rodrigo.springcloud.msusers.infra.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import rodrigo.springcloud.msusers.domain.User;

public interface UserRepository extends JpaRepository<User, UUID> {
    
    Optional<User> findByEmail(String email);

    Optional<User> findByIdAndDeletedAtIsNull(UUID uuid);

    @Query(
        "SELECT u "
        +   "FROM User u "
        +   "JOIN FETCH u.address "
        +   "WHERE u.deletedAt IS NULL "
        +       "AND u.id = :id"
    )
    Optional<User> findByIdAndDeletedAtIsNullWithFetchAddress(@Param("id") UUID uuid);
}
